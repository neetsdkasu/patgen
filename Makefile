# Makefile

# Open Watcom C++
CC=cl
CMSW=-TP -Fe$.

# Borland C++
CC=bcc32
CMSW=-P -e$.

SRC=div.cpp
DEP=$(SRC)
TARGET=$(SRC:.cpp=.exe)
SW=

PG_SRC=patgen.cpp
PG_DEP=$(PG_SRC) $(PG_SRC:.cpp=.hpp) mydef.hpp
PG_DST=$(PG_SRC:.cpp=.exe)
PG_SW=

AllFiles: $(TARGET) $(PG_DST)

foo.exe: foo.cpp foo.hpp mydef.hpp

$(PG_DST): $(PG_DEP)
	$(CC) $(CMSW) $(PG_SW) $(PG_SRC)

$(TARGET): $(DEP)
	$(CC) $(CMSW) $(SW) $(SRC)

clean:
	if exist *.obj del *.obj
	if exist *.tds del *.tds
	if exist *.exe del *.exe