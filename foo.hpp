#ifndef _FOO_HPP_
#define _FOO_HPP_

#include <iostream>
#include <cstring>

#include "mydef.hpp"

__BEGIN_MYNAMESPACE__

class PatternGeneratorException
{
};

class PatternGenerator
{
private:
	int *_src;
	bool *_flag;
	int *_list;
	int *_indexes;
	int _srcsize;
	int _len;
	int _count;
	
	PatternGenerator();
	void InitCommon() throw(PatternGeneratorException);
	
	
public:
	
	PatternGenerator(int srcsize, int len) throw(PatternGeneratorException);
	PatternGenerator(int size) throw(PatternGeneratorException);
	PatternGenerator(const int *sortedsrc, int srcsize, int len) throw(PatternGeneratorException);
	~PatternGenerator();
	
	const int* Source() const { return _src; }
	
	void Reset();
};

PatternGenerator::PatternGenerator(int srcsize, int len) throw(PatternGeneratorException)
		: _srcsize(srcsize), _len(len), _count(0), 
			_src(NULL), _flag(NULL), _list(NULL), _indexes(NULL) {
	InitCommon();
}

PatternGenerator::PatternGenerator(int size) throw(PatternGeneratorException)
		: _srcsize(size), _len(size), _count(0),
			_src(NULL), _flag(NULL), _list(NULL), _indexes(NULL) {
	InitCommon();
}

PatternGenerator::PatternGenerator(const int *sortedsrc, int srcsize, int len) throw(PatternGeneratorException)
		: _srcsize(srcsize), _len(len), _count(0),
			_src(NULL), _flag(NULL), _list(NULL), _indexes(NULL) {
	InitCommon();
	using namespace std;
	memcpy(_src, sortedsrc, sizeof(int) * _srcsize);
	
}

PatternGenerator::~PatternGenerator() {
	delete [] _src;
	delete [] _flag;
	delete [] _list;
	delete [] _indexes;
}

void PatternGenerator::InitCommon() throw(PatternGeneratorException) {
	if (_srcsize < 2 || _len < 2 || _len < _srcsize) {
		throw PatternGeneratorException();
	}
	_src = new int[_srcsize];
	_flag = new bool[_srcsize];
	_list = new int[_len];
	_indexes = new int[_len];
	
	for (int i = 0; i < _srcsize; i++) {
		_src[i] = i;
		_flag[i] = false;
	}
}

void PatternGenerator::Reset() {
	for (int i = 0; i < _len; i++) {
		
	}
}

__END_MYNAMESPACE__

#endif // _FOO_HPP_