#include <iostream>

#include "foo.hpp"


int main() {
	
	try {
	
		myapp::PatternGenerator foo(5);
		
		
		std::cout << "finish" << std::endl;
	}
	catch (myapp::PatternGeneratorException& ex) {
		std::cout << "catch" << std::endl;
	}
	catch (...) {
		std::cout << "Unknown" << std::endl;
	}
	
	return 0;
}

