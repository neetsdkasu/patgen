#include <iostream>

#include "patgen.hpp"

using namespace std;
using namespace _NS_MYAPP_PATGEN_;

void test1() {
	cout << "## test 1 ##" << endl;
	
	try {
		PatternGenerator gen(-1);
		cout << "No catch PatternGeneratorException!" << endl;
	}
	catch (PatternGeneratorException& ex) {
		cout << "PatternGeneratorException!" << endl;
	}
}

void test2() {
	cout << "## test 2 ##" << endl;
	
	
	cout << "nCr Test" << endl;
	for (int i = 0; i <= 10; i++) {
		cout << "(10,";
		cout.fill(' ');
		cout.width(3);
		cout << i << ") => " << nCr(10, i) << endl;
	}
	
	cout << "nPr Test" << endl;
	for (int i = 0; i <= 10; i++) {
		cout << "(10,";
		cout.fill(' ');
		cout.width(3);
		cout << i << ") => " << nPr(10, i) << endl;
	}
}

void test3() {
	cout << "## test 3 ##" << endl;
	
	PatternGenerator gen(3);
	
	cout << "Source: ";
	const int *kk = gen.Source();
	for (int i = 0; i < gen.SourceSize(); i++) {
		cout << "0123456789ABCDEF"[kk[i]] << ", ";
	}
	cout << endl;
	
	for (int j = 0; j <= 10; j++) {
		const int* k = gen.Next();
		cout.fill('.');
		cout.width(4);
		cout << gen.Count() << ": ";
		for (int i = 0; i < gen.Length(); i++) {
			cout << "0123456789ABCDEF"[k[i]];
		}
		cout << endl;
	}
}

void test4() {
	cout << "## test 4 ##" << endl;
	PatternGenerator gen(4);
	
	cout << "Source: ";
	const int *kk = gen.Source();
	for (int i = 0; i < gen.SourceSize(); i++) {
		cout << "0123456789ABCDEF"[kk[i]] << ", ";
	}
	cout << endl;
	
	while (gen.HasNext()) {
		const int* k = gen.Next();
		cout.fill('+');
		cout.width(4);
		cout << gen.Count() << ": ";
		for (int i = 0; i < gen.Length(); i++) {
			cout << "0123456789ABCDEF"[k[i]];
		}
		cout << endl;
	}
}

void test5() {
	cout << "## test 5 ##" << endl;
	PatternGenerator gen(5, 3);
	
	cout << "Source: ";
	const int *kk = gen.Source();
	for (int i = 0; i < gen.SourceSize(); i++) {
		cout << "0123456789ABCDEF"[kk[i]] << ", ";
	}
	cout << endl;
	
	while (gen.HasNext()) {
		const int* k = gen.Next();
		cout.fill('-');
		cout.width(4);
		cout << gen.Count() << ": ";
		for (int i = 0; i < gen.Length(); i++) {
			cout << "0123456789ABCDEF"[k[i]];
		}
		cout << endl;
	}
}

void test6() {
	cout << "## test 6 ##" << endl;
	
	int src[] = { 0, 0, 3, 7};
	int len = sizeof(src) / sizeof(src[0]);
	
	PatternGenerator gen(src, len, len);
	
	cout << "Source: ";
	const int *kk = gen.Source();
	for (int i = 0; i < gen.SourceSize(); i++) {
		cout << "0123456789ABCDEF"[kk[i]] << ", ";
	}
	cout << endl;
	
	while (gen.HasNext()) {
		const int* k = gen.Next();
		cout.fill('*');
		cout.width(4);
		cout << gen.Count() << ": ";
		for (int i = 0; i < gen.Length(); i++) {
			cout << "0123456789ABCDEF"[k[i]];
		}
		cout << endl;
	}
}

void test7() {
	cout << "## test 7 ##" << endl;
	
	int src[] = { 1, 3, 5, 6, 6};
	int len = sizeof(src) / sizeof(src[0]);
	
	PatternGenerator gen(src, len, 3);

	cout << "Source: ";
	const int *kk = gen.Source();
	for (int i = 0; i < gen.SourceSize(); i++) {
		cout << "0123456789ABCDEF"[kk[i]] << ", ";
	}
	cout << endl;
	
	while (gen.HasNext()) {
		const int* k = gen.Next();
		cout.fill('_');
		cout.width(4);
		cout << gen.Count() << ": ";
		for (int i = 0; i < gen.Length(); i++) {
			cout << "0123456789ABCDEF"[k[i]];
		}
		cout << endl;
	}
}

int main() {
	
	try {
		test1();
		
		test2();
		
		test3();
		
		test4();
		
		test5();
		
		test6();
		
		test7();
	}
	catch (PatternGeneratorException& ex) {
		cout << "Catch PatternGeneratorException in main()!!" << endl;
	}
	catch (...) {
		cout << "Catch Unknown Exception in main()!!" << endl;
	}
	
	return 0;
} 

