#include <iostream>

class Foo
{
private:
	int _x;
	int _y;
public:
	int z;
	
	Foo() : _x(0), _y(1), z(2) {}
};

int main() {
	using namespace std;
	
	int (Foo::*zz) = &Foo::z;
	
	Foo foo;
	
	cout << (foo.*zz) << endl;
	
	cout << "Hello World" << endl;

	return 0;
}
