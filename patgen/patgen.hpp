#pragma once
#ifndef __PATGEN_PATGEN_HPP__
#define __PATGEN_PATGEN_HPP__

#include <cstring>
#include <iostream>

#include "..\common\mydef.hpp"
#include "..\memory\nullptr.hpp"
#include "math.hpp"

using namespace _NS_MYAPP_MEMORY_;

__BEGIN_MYNAMESPACE_MYAPP_PATGEN__


class PatternGeneratorException {};

class PatternGenerator
{
public:
	
	PatternGenerator(int srcsize, int len) throw(PatternGeneratorException);
	PatternGenerator(int size) throw(PatternGeneratorException);
	PatternGenerator(const int *src, int srcsize, int len) throw(PatternGeneratorException);
	~PatternGenerator();
	
	int Count() const { return _count; }
	int Length() const { return _len; }
	int SourceSize() const { return _srcsize; }
	const int* Source() const { return _src; }
	
	
	int AllPatternCount();
	bool HasNext() const;
	void Reset();
	const int* Next();
	
	
private:
	
	PatternGenerator() {}
	PatternGenerator(const PatternGenerator&) {}
	
	void InitCommon() throw(PatternGeneratorException);
	
	bool * _flag;
	int  * _src;
	int  * _list;
	int  * _indexes;
	bool   _hasnext;
	int    _srcsize;
	int    _len;
	int    _count;
	int    _all;
};


//////////////////////////////////////////////////////////////////////// 
// Codes
//////////////////////////////////////////////////////////////////////// 

void PatternGenerator::InitCommon() throw(PatternGeneratorException) {
	if (_srcsize < 2 || _len < 2 || _srcsize < _len) {
		throw PatternGeneratorException();
	}
	
	_src = new int[_srcsize];
	_flag = new bool[_srcsize];
	
	_list = new int[_len];
	_indexes = new int[_len];
	
	memset(_flag, 0, _srcsize * sizeof(_flag[0]));
	
	memset(_indexes, 0, _len * sizeof(_indexes[0]));
	memset(_list, 0, _len * sizeof(_list[0]));
	
}

PatternGenerator::PatternGenerator(int srcsize, int len) throw(PatternGeneratorException)
		: _srcsize(srcsize), _len(len), _all(0), _count(0), _hasnext(true),
			_src(nullptr), _flag(nullptr), _list(nullptr), _indexes(nullptr) {
	InitCommon();
	for (int i = 0; i < _srcsize; i++) {
		_src[i] = i;
	}
	Reset();
}

PatternGenerator::PatternGenerator(int size) throw(PatternGeneratorException)
		: _srcsize(size), _len(size), _all(0), _count(0), _hasnext(true),
			_src(nullptr), _flag(nullptr), _list(nullptr), _indexes(nullptr) {
	InitCommon();
	for (int i = 0; i < _srcsize; i++) {
		_src[i] = i;
	}
	Reset();
}

PatternGenerator::PatternGenerator(const int *sortedsrc, int srcsize, int len) throw(PatternGeneratorException)
		: _srcsize(srcsize), _len(len), _all(0), _count(0), _hasnext(true),
			_src(nullptr), _flag(nullptr), _list(nullptr), _indexes(nullptr) {
	InitCommon();
	memcpy(_src, sortedsrc, sizeof(int) * _srcsize);
	Reset();
}

inline
PatternGenerator::~PatternGenerator() {
	delete [] _src;
	delete [] _flag;
	delete [] _list;
	delete [] _indexes;
}

void PatternGenerator::Reset() {
	for (int i = 0; i < _len; i++) {
		_flag[_indexes[i]] = false;
	}
	for (int i = 0; i < _len; i++) {
		_indexes[i] = i;
		_flag[_indexes[i]] = true;
	}
	_hasnext = true;
	_count = 0;
}

inline
bool PatternGenerator::HasNext() const {
	return _hasnext;
}

const int* PatternGenerator::Next() {
	int i, j, b;
	bool f;
	
	if (_hasnext == false) {
		return _list;
	}
	
	for (i = 0; i < _len; i++) {
		_list[i] = _src[_indexes[i]];
	}
	
	i = _len - 1;
	_flag[_indexes[i]] = false;
	 
	f = true;
	 
	for (;;) {
		if (f) {
			b = _src[_indexes[i]];
			for (j = _indexes[i] + 1; j < _srcsize; j++) {
				if (_flag[j] == false && _src[j] != b) {
					break;
				}
			}
		} else {
			for (j = 0; j < _srcsize; j++) {
				if (_flag[j] == false) {
					break;
				}
			}
			f = true;
		}
		if (j == _srcsize) {
			i--;
			if (i < 0) {
				_hasnext = false;
				break;
			} else {
				_flag[_indexes[i]] = false;
			}
		} else {
			_indexes[i] = j;
			_flag[_indexes[i]] = true;
			i++;
			if (i == _len) {
				break;
			} else {
				f = false;
			}
		}
	}
	_count++;

	return _list;
}


int PatternGenerator::AllPatternCount() {
	return _all;
}


__END_MYNAMESPACE_MYAPP_PATGEN__

#endif // __PATGEN_PATGEN_HPP__
