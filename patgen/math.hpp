#pragma once
#ifndef __PATGEN_MATH_HPP__
#define __PATGEN_MATH_HPP__

#include "..\common\mydef.hpp"

__BEGIN_MYNAMESPACE_MYAPP_PATGEN__

template<class T>
extern T nCr(T n, T r) {
	if (n < (T)0 || r < (T)0 || n < r) {
		return (T)0;
	}
	if (n - r < r) {
		r = n - r;
	}
	T c = (T)0;
	for (T i = (T)1; i <= r; i++) {
		c *= n;
		c /= i;
		n--;
	}
	return c;
}

template<class T>
extern T nPr(T n, T r) {
	if (n < (T)0 || r < (T)0 || n < r) {
		return (T)0;
	}
	T p = (T)1;
	while (r > (T)0) {
		p *= n;
		n--;
		r--;
	}
	return p;
}


__END_MYNAMESPACE_MYAPP_PATGEN__

#endif // __PATGEN_MATH_HPP__
