# Common Makefile

!ifdef CP_WATCOM

# Open Watcom C++
CC=cl
CMSW=-TP -Fe$.

!else

# Borland C++
CC=bcc32
CMSW=-P -e$.

!endif

MYDEF=common\mydef.hpp

