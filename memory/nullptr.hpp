#pragma once
#ifdef __OLD_COMPILER__
#ifndef __NULLPTR_HPP__
#define __NULLPTR_HPP__

#include "..\common\mydef.hpp"

__BEGIN_MYNAMESPACE_MYAPP_MEMORY__

struct nullptr_t
{
	template<typename T, typename C>
	operator T C::*() const { return 0; }
	
	template<typename T>
	operator T*() const { return 0; }
	
private:
	
	void operator &() const;
};

inline nullptr_t __get_nullptr_t() { return nullptr_t(); }

__END_MYNAMESPACE_MYAPP_MEMORY__

inline bool operator == (const _NS_MYAPP_MEMORY_::nullptr_t&, const _NS_MYAPP_MEMORY_::nullptr_t&) { return true;  }
inline bool operator != (const _NS_MYAPP_MEMORY_::nullptr_t&, const _NS_MYAPP_MEMORY_::nullptr_t&) { return false; }

#define nullptr (_NS_MYAPP_MEMORY_::__get_nullptr_t())

#endif // __NULLPTR_HPP__
#endif // __OLD_COMPILER__
