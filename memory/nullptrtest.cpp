#include <iostream>
#include <cassert>

#include "..\common\mydef.hpp"
#include "nullptr.hpp"

using namespace std;
using namespace _NS_MYAPP_MEMORY_;

class Foo {};

void testfunc1(int)  { cout << "testfunc1 int"  << endl; }
void testfunc1(int*) { cout << "testfunc1 int*" << endl; }

void testfunc2(int)  { cout << "testfunc2 int"  << endl; }
void testfunc2(int&) { cout << "testfunc2 int&" << endl; }

void testfunc3(int*)      { cout << "testfunc3 int*"      << endl; }
void testfunc3(nullptr_t) { cout << "testfunc3 nullptr_t" << endl; }

int main() {
	
	testfunc1(nullptr);
	
	// testfunc2(nullptr);
	
	testfunc3(nullptr);
	
	void *p = nullptr;
	void (Foo::*fp) = nullptr;
	
	assert( ( nullptr == nullptr ) == true  );
	assert( ( nullptr != nullptr ) == false );
	
	cout << sizeof(nullptr) << endl;
	
	return 0;
}